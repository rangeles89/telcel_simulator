'''
Created on 11 oct. 2016

@author: ricardo
'''
from __future__ import absolute_import
from gevent.monkey import patch_all; patch_all()
from gevent.server import StreamServer
import logging
import gevent
from gevent import sleep
from gevent.queue import Queue  
from re import compile, match
from datetime import datetime, timedelta
import redis

STX = b'\x02'
ETX = b'\x03'
MAX_SECURE_LENGHT = 200
logger = logging.getLogger(__name__)

FIXED_PART_RESPONSE_MESSAGE_PATTERN = r"(?P<action>[0-9]{2})(?P<id>[a-zA-Z0-9]{8})(?P<date>[0-9]{8})(?P<time>[0-9]{6})"
fixed_parser_object = compile(FIXED_PART_RESPONSE_MESSAGE_PATTERN) 
FIXED_PART_LEN = 2 + 8 + 8 + 6

PARSE_BASE_PATTERN = r"(?P<chain_id>[a-zA-Z0-9 ]{10})(?P<store_id>[a-zA-Z0-9 ]{5})(?P<pos_id>[a-zA-Z0-9 ]{10})(?P<pos_time>[0-9]{6})(?P<pos_date>[0-9]{8})(?P<folio>[0-9]{10})(?P<phone_number>[0-9]{10})(?P<amount>[0-9]{10})"
dynamic_parser_object = compile(PARSE_BASE_PATTERN)

FORMAT_RESP_ABONO_TIEMPO_AIRE = "{chain_id:10.10}{store_id:5.5}{pos_id:10.10}{pos_time}{pos_date}{folio}{phone_number}{amount:0>10}{response_code}{txn_number:0>6}"

FIXED_PART_REQUEST_MESSAGE = "{action:0>02}{id}{date}{time}"
"""Actions"""
REQ_ABONO_TIEMPO_AIRE  = "01"
RESP_ABONO_TIEMPO_AIRE = "02"

REQ_ECO_SALIDA  = "96"
RESP_ECO_SALIDA = "97"

REQ_ECO_ENTRADA  = "98"
RESP_ECO_ENTRADA = "99"



"""Actions description"""
def get_actions_description():
    return {REQ_ABONO_TIEMPO_AIRE : "Solicitud abono tiempo aire",
            RESP_ABONO_TIEMPO_AIRE : "Respuesta abono tiempo aire",
            REQ_ECO_SALIDA : "Solicitud eco Telcel-Switch",
            RESP_ECO_SALIDA : "Respuesta eco Switch-Telcel",
            REQ_ECO_ENTRADA : "Solicitud eco Switch-Telcel",
            RESP_ECO_ENTRADA : "Respuesta eco Telcel-Switch" }



ACTION_DESC = get_actions_description()

redis_instance = redis.StrictRedis()

response_codes = ["00", "01", "02", "03", "04", "05", "06", "07", "08", "09"]

def parse_action(action):
    if action == 'None':
        return None
    
    return action.split(" ")

def process_phone_number(phone_number):
    key = 'telcel_simulator.phone_rules'
    check_list = [str(item,'utf-8') for item in redis_instance.lrange(key, 0, -1)]
    
    rule = {
            'response_code':'00',
            'action_after_receive':'None',
            'action_before_send':'None'
            }
    
    for rule_s in check_list:
        phone, response_code, action_after_receive, action_before_send = rule_s.split('-')
        if phone in phone_number:
            rule['response_code'] = response_code
            rule['action_after_receive'] = parse_action(action_after_receive)
            rule['action_before_send'] = parse_action(action_before_send)
            
    return rule

class TelcelStreamServer(StreamServer):
    '''
    classdocs
    '''
    logger = logging.getLogger(__name__)
    def txn_number(self):
        return redis_instance.hincrby("telcel_simulator", "txn_number", 3)
    
    def set_last_echo_datetime(self, address, current_datetime):
        #self.logger.info("saving echo data %s"%current_datetime)
        last_echo_date = current_datetime[:8]
        last_echo_time = current_datetime[8:]
        redis_instance.hset("telcel_simulator", "last_eco_date%s"%address, last_echo_date)
        redis_instance.hset("telcel_simulator", "last_eco_time%s"%address, last_echo_time)
        last_eco_datetime = datetime.strptime(current_datetime, "%d%m%Y%H%M%S")
        self.last_eco = last_eco_datetime
        
    def get_last_eco(self):
        try:
            return self.last_eco
        except:
            return None
    
    def verify_eco_datetime(self,address, current_eco_datetime):
        last_eco_date = redis_instance.hget("telcel_simulator", "last_eco_date%s"%address)
        last_eco_time = redis_instance.hget("telcel_simulator", "last_eco_time%s"%address)
        
        if len(last_eco_date) == 0:
            #self.logger.info("no last echo")
            self.set_last_echo_datetime(address, current_eco_datetime)
            return True
        
        last_eco_datetime = str(last_eco_date+last_eco_time, 'utf-8')
        last_eco_datetime = datetime.strptime(last_eco_datetime, "%d%m%Y%H%M%S")
        self.last_eco = last_eco_datetime
        
        eco_datetime = datetime.strptime(current_eco_datetime, "%d%m%Y%H%M%S")
        self.set_last_echo_datetime(address, current_eco_datetime)
        diff = eco_datetime - last_eco_datetime
        
        if  diff != timedelta(seconds=180):
            self.logger.warning("[{0}] - wrong echo timing, difference = {1} , expected = 180".format(address, diff.total_seconds()))
            return False
        
        return True
    
    def set_session_txn_number(self, count):
        redis_instance.hset("telcel_simulator", "session_txn_number", count)
    
    def set_session_echo_received_number(self, count):
        redis_instance.hset("telcel_simulator", "session_echo_received", count)
    
    def set_session_echo_sent_number(self, count):
        redis_instance.hset("telcel_simulator", "session_echo_sent", count)
    
    def set_session_response_count(self, response_code, count):
        redis_instance.hset("telcel_simulator", "session_resp_count_%s"%response_code, count)
    
    def set_counters_to_zero(self):
        self.set_session_txn_number(0)
        self.set_session_echo_received_number(0)
        self.set_session_echo_sent_number(0)
        for rc in response_codes:
            self.set_session_response_count(rc, 0)
    
    def inc_session_txn_number(self):
        redis_instance.hincrby("telcel_simulator", "session_txn_number", 1)
    
    def inc_session_echo_received(self):
        redis_instance.hincrby("telcel_simulator", "session_echo_received", 1)
    
    def inc_session_echo_sent(self):
        redis_instance.hincrby("telcel_simulator", "session_echo_sent", 1)
    
    def inc_session_response(self, response_code):
        redis_instance.hincrby("telcel_simulator", "session_resp_count_%s"%response_code, 1)
    
    def get_session_txn_number(self):
        r = redis_instance.hget("telcel_simulator", "session_txn_number")
        return int(str(r, 'utf-8'))
    
    def get_session_echo_received_number(self):
        r = redis_instance.hget("telcel_simulator", "session_echo_received")
        return int(str(r, 'utf-8'))
    
    def get_session_echo_sent_number(self):
        r = redis_instance.hget("telcel_simulator", "session_echo_sent")
        return int(str(r, 'utf-8'))
    
    def get_session_response_count(self, response_code):
        r = redis_instance.hget("telcel_simulator", "session_resp_count_%s"%response_code)
        return int(str(r, 'utf-8'))
    
    def add_connection(self, address):
        redis_instance.lpush("telcel_simulator.active_connections", "{0}".format(address))
    
    def remove_connection(self, address):
        redis_instance.lrem("telcel_simulator.active_connections", 1, "{0}".format(address))
    
    def get_connections(self):
        return [str(c, 'utf-8') for c in redis_instance.lrange("telcel_simulator.active_connections", 0, -1)]
    
    def get_connections_number(self):
        try:
            return redis_instance.llen("telcel_simulator.active_connections")
        except:
            return 0
    
    def add_log(self, log_str):
        redis_instance.lpush("telcel_simulator.short_log", log_str)
        redis_instance.ltrim("telcel_simulator.short_log", 0, 10)
    
    def get_log(self):
        try:
            return [str(l, 'utf-8') for l in redis_instance.lrange("telcel_simulator.short_log", 0, -1)]
        except:
            return []
    
    def flush_connections(self):
        while redis_instance.lpop("telcel_simulator.active_connections"):
            pass
    
    def set_amount(self, amount):
        redis_instance.hset("telcel_simulator", "session_amount", amount)
    
    def add_amount(self, amount):
        redis_instance.hincrby("telcel_simulator", "session_amount", amount)
    
    def get_amount(self):
        amount = redis_instance.hget("telcel_simulator", "session_amount")
        return int(str(amount, 'utf-8'))
    
    def __init__(self, address='0.0.0.0', port=5556):
        '''
        Constructor
        '''
        #self.logger = logging.getLogger(__name__)
        
        super(TelcelStreamServer, self).__init__((address, port), self.connection_handler)
        self.flush_connections()
        redis_instance.hset("telcel_simulator", "counter", 0)
        self.set_counters_to_zero()
        self.set_amount(0)
        
        self.queues_dict = {}
    
    def connection_handler(self, socket, address):
        stop_event = gevent.event.Event()
        self.logger.info('Client {0} connected, serving socket {1}'.format(address, str(socket)))
        self.add_connection(address)
        io_watcher = gevent.hub.get_hub().loop.io(socket.fileno(), 1)
        #gevent.socket.wait_read(self.connection.fileno())
        self.logger.info("wait io {0}".format(io_watcher))
                
                
        
        redis_instance.hset("telcel_simulator", "last_eco_date%s"%address[0], "")
        redis_instance.hset("telcel_simulator", "last_eco_time%s"%address[0], "")
        self.queues_dict[address] = Queue()
        
        g1 = gevent.spawn(self.receive_thread, io_watcher, address, self.queues_dict[address],socket, stop_event)
        #gevent.spawn(self.send_thread, self.queues_dict[address], socket, address)
        g2 = gevent.spawn(self.echo_to_client_thread,address,socket, stop_event)
        
        #continue_loop = True
    
        #while continue_loop:
        #    gevent.sleep(0.001)
        gevent.wait([stop_event,])
        gevent.socket.cancel_wait(io_watcher)
        #gevent.kill(g1)
        #gevent.kill(g2)
        gevent.joinall([g1, g2])
        
        self.logger.info("Finishing connection with {0}".format(address))
        socket.close()
        self.remove_connection(address)
            
        return 0
    
    def receive_thread(self, io, address, queue, socket, stop_event):
        self.logger.info("Waiting data from {0}...".format(address))
        while True:
            if stop_event.is_set():
                break
            try:
                gevent.socket.wait(io)
                transaction_stream = self.read_basic_stream(socket)
            except:
                transaction_stream = None
            
            if not transaction_stream:
                stop_event.set()
                self.logger.info("[{0}] - Disconnecting".format(address))
                break
            self.logger.info("[{0}] - transaction received {1}".format(address, transaction_stream))
            fixed_part = transaction_stream[:FIXED_PART_LEN]
            dynamic_part = transaction_stream[FIXED_PART_LEN:]
            if len(dynamic_part) == 0:
                dynamic_part = None
            
            parsed_fixed_part = self.parse_fixed_part(fixed_part)
            self.logger.info('[{0}] - Received ID:{id} - Action:{action} - Date:{date} - Time:{time}'.format(address, **parsed_fixed_part))
            gevent.spawn(self.manage_transaction, parsed_fixed_part['action'], parsed_fixed_part['id'], parsed_fixed_part, dynamic_part, address, queue, socket, stop_event)
            sleep(0)
        
        self.logger.info("[{0}] - No waiting for data anymore".format(address))
        
            
    
    def manage_transaction(self, action, id, fixed_part, dynamic_part, address, queue, socket, stop_event):
        s = '[{0}] - Managing transaction {1}:{2} ID:{3}'.format(address, action, ACTION_DESC[action], id)   
        self.logger.info(s)
        self.add_log(s)
        delay = None
        disconnect_ = False
        if action == REQ_ECO_ENTRADA:
            """Send eco response"""
            if self.verify_eco_datetime(address[0], fixed_part['date'] + fixed_part['time']) == False:
                self.logger.info("Verify echo timing!")
            
            fixed_part['action'] = RESP_ECO_ENTRADA
            response = FIXED_PART_REQUEST_MESSAGE.format(**fixed_part)
        elif action == RESP_ECO_SALIDA:
            """Do nothing"""
            response = None
        elif action == REQ_ABONO_TIEMPO_AIRE:
        
            parsed_dynamic_part = None
            if len(dynamic_part)>0:
                parsed_dynamic_part = self.parse_sale_dynamic_part(dynamic_part)
                logstr = ''
                for k,v in parsed_dynamic_part.items():
                    logstr += "%s:%s, "%(k,v)
                s = '[{0}] - Dynamic part {1}'.format(id, logstr[:-2])
                self.logger.info(s)
                self.add_log(s)   
                fixed_part['action'] = RESP_ABONO_TIEMPO_AIRE
                response = FIXED_PART_REQUEST_MESSAGE.format(**fixed_part)
                
                rule = process_phone_number(parsed_dynamic_part['phone_number'])
                
                parsed_dynamic_part['response_code'] = rule['response_code']
                parsed_dynamic_part['txn_number'] = self.txn_number()
                
                if rule['action_before_send'] is None:
                    pass
                elif rule['action_before_send'][0] == 'delay':
                    delay = float(rule['action_before_send'][1])
                elif rule['action_before_send'][0] == 'disconnect':
                    disconnect_ = True
                
                response = response + (FORMAT_RESP_ABONO_TIEMPO_AIRE.format(**parsed_dynamic_part))
                
            else:
                self.logger.error('[{0}] - Bad transaction'.format(id))   
                response = None
                

                
        if response:
            #sleep(6.2)
            if delay:
                sleep(delay)
            
            if disconnect_:
                #socket.close()
                stop_event.set()
                return 0
            s = '[{0}] - Sending response action:{1}({2}) : {3}'.format(id, fixed_part['action'], ACTION_DESC[fixed_part['action']], response)
            self.logger.info(s)
            self.add_log(s)
            data = STX + bytes(response, 'utf-8') + ETX
            
            try:
                socket.sendall(data)
                if fixed_part['action'] == RESP_ECO_ENTRADA:
                    self.inc_session_echo_received()
                elif fixed_part['action'] == RESP_ABONO_TIEMPO_AIRE:
                    self.inc_session_txn_number()
                    self.inc_session_response(parsed_dynamic_part['response_code'])
                    if parsed_dynamic_part['response_code'] == '00':
                        self.add_amount(int(parsed_dynamic_part['amount']))
            except:
                self.logger.info('[{0}] - Connection lost before/while sending {1}'.format(address, id))
                stop_event.set()
               
        
        return 0
    
    def parse_sale_dynamic_part(self, dynamic_part):
        dynamic_part = dynamic_parser_object.match(dynamic_part)
        sleep(0)
        fields={}
        for k,v in dynamic_part.groupdict().items():
            fields[k] = v
        return fields
            
    def parse_fixed_part(self, fixed_part):        
        fixed_part = fixed_parser_object.match(fixed_part)
        sleep(0)
        fields={}
        for k,v in fixed_part.groupdict().items():
            #self.logger.info("Found \'%s\' : \'%s\' in fixed part"%(k,v))
            fields[k] = v
            sleep(0)
        return fields
    
    def read_basic_stream(self, fd):
        stx = fd.recv(1)
        
        if not stx:
            return None
        
        if stx != STX:
            return None
        transaction_stream = b''
        b = b''
        while b != ETX:
            b = fd.recv(1)
            transaction_stream += b
            if len(transaction_stream) > MAX_SECURE_LENGHT:
                return None
        
        return str(transaction_stream[:-1], 'utf-8')
        
    
    def send_thread(self, queue, socket, address, stop_event):
        self.logger.info("Ready to send to {}...".format(address))
        while True:
            if not queue.empty():
                data = queue.get(block=False)
                data = STX + bytes(data, 'utf-8') + ETX
                socket.sendall(data)
            else:
                sleep(.003)
    
    def echo_to_client_thread(self, address,socket, stop_event):
        self.logger.info("raising echos")
        while True:
            if stop_event.is_set():
                break
            consecutive = redis_instance.hincrby("telcel_simulator","counter", 1)
            now = datetime.now().strftime("%d%m%Y%H%M%S")
            
            fields = {
                      'id': "{two_digit:0>2}{consecutive:0=6}".format(
                                                                      two_digit="TL",
                                                                      consecutive=consecutive),
                      'action': REQ_ECO_SALIDA,
                      'date':now[:8],
                      'time':now[8:]
                      }
            echo = FIXED_PART_REQUEST_MESSAGE.format(**fields)
            data = STX + bytes(echo, 'utf-8') + ETX
            socket.sendall(data)
            self.inc_session_echo_sent()
            #sleep(180)
            gevent.wait([stop_event,], 180)
        self.logger.info("stop sending echos")
        