'''
Created on 11 oct. 2016

@author: ricardo
'''
from __future__ import absolute_import
from gevent import monkey; monkey.patch_all();
import gevent;
from telcel.server import TelcelStreamServer, response_codes
import logging
from pprint import pformat
from datetime import datetime, timedelta
logging.basicConfig(filename='telcel_sim.log',level=logging.INFO,format = '%(asctime)s-%(levelname)s-%(message)s')
logger = logging.getLogger(__name__)

telcel_responses = {
                    "00": "Aprobada",
                    "01": "Teléfono no Valido.",
                    "02": "Destino no disponible (No se pudo abonar).",
                    "03": "Monto no Valido.",
                    "04": "Teléfono no susceptible de Abono.",
                    "05": "Crédito no disponible.",
                    "06": "Mantenimiento Telcel en curso.",
                    "07": "Rechazo por tabla de transacciones llena.",
                    "08": "Rechazo por time-out interno.",
                    "09": "Autorizador no disponible.",
                    "102": "Problema interno, abono no realizado.",
                    "109": "No se pudo contactar al host especificado.",
                    "129": "Sin respuesta de autorizador. Tiempo excedido.",
                    "139": "Sin respuesta de autorizador. Desconexión."
                    }

def screen(server, start):
    _now = datetime.now()
    elapsed = _now - start
    print("Simulador Telcel - Tiempo: {0}".format(str(elapsed)))
    print("Conexiones activas : %d"%server.get_connections_number())
    for connection in server.get_connections():
        print("\t%s"%connection)
    
    echoes_received, echoes_sent, txns =  server.get_session_echo_received_number(), server.get_session_echo_sent_number(), server.get_session_txn_number()   
    
    print("Ecos recibidos: %8d\t Ecos enviados:%8d\tTransacciones:%8d"%(echoes_received, echoes_sent, txns))
    
    echo = server.get_last_eco()
    if echo:
        print("Ultimo echo: {0} - {1}".format(echo.strftime("%d/%m/%Y %H:%M:%S"), str(_now - echo)))
    
    print("Monto total por transacciones aprobadas ${0:,.2f}".format(server.get_amount()/100.0 ))
    
    print("Métricas por código de respuesta:")
    print("{0} {1:>8} ({2:>9}) - ({3})".format("Código", "#", "%", "Descripción"))
    for rc in response_codes:
        nrc = server.get_session_response_count(rc)
        if not txns:
            txns = 1
        s = "{0:6} {1:>8} ({2:>9.4f} %) - ({3})".format(rc, nrc, nrc/txns * 100.0, telcel_responses[rc])
        print(s)
    print("=============================================================================================")
    print("Log:")
    for l in server.get_log():
        print(l)
    print("=============================================================================================")

import sys

def main():
    if len(sys.argv)>1:
        port = int(sys.argv[1])
    else:
        port = 5556
    
    server = TelcelStreamServer(port = port)
    server_thread = gevent.spawn(server.serve_forever)
    start = datetime.now()
    logger.info("Hi")
    while True:
        print(chr(27) + "[2J")
        screen(server, start)
        gevent.sleep(1)

if __name__ == '__main__':
    main()