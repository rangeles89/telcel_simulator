TELCEL Simulator
================

This simulator implements TELCEL's service sale protocol, in order to be able to test remote clients before deployment.

It's possible to emulate different responses according to different condicios (for example phone numbers).

It requires a running instance of redis in order to store transactions information.

It runs on terminal, in order to monitor all the diferent requests arriving.